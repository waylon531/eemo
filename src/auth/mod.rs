//    eemo, a server for an electrical engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

mod schema;
mod models;

use diesel;
use diesel::associations::HasTable;
use diesel::prelude::*;
use diesel::pg::PgConnection;
use dotenv::dotenv;
use sha3::{Digest, Sha3_256};

use std::env;

use libeemo::AuthError;

use self::models::{User,NewUser};
use self::schema::auth::dsl::*;

pub fn establish_connection() -> Result<PgConnection,AuthError> {
    dotenv().ok();

    let database_url = match env::var("DATABASE_URL") {
        Ok(url) => url,
        Err(_) => return Err(AuthError::EnvError)
    };
    match PgConnection::establish(&database_url) {
        Ok(conn) => Ok(conn),
        Err(_) => Err(AuthError::DBError)
    }
}

pub fn auth_user(user: String,pass: String) -> Result<(),AuthError> {
    if user.starts_with("Guest") {
        return Err(AuthError::UsernameReserved);
    }
    let connection = establish_connection()?; 
    //select users with same username
    match auth.filter(username.eq(&user)).get_results::<User>(&connection) {
        Ok(users) => {
            if users.len() > 1 {
                Err(AuthError::MultipleUsers)
            } else if users.len() == 0 {
                Err(AuthError::UserNotFound)
            } else {
                UnhashedUser{username: user, password: pass}.auth(&users[0])
            }
        },
        //error out if multiple users have same username
        //check users password
        Err(_) => Err(AuthError::DBError)
    }
}

pub fn add_user(user: String, pass: String) -> Result<(),AuthError> {
    let conn = establish_connection()?;
    let mut hasher = Sha3_256::default();
    hasher.input(pass.as_bytes());
    let hash = hasher.result()[..].to_vec();
    match diesel::insert(&NewUser {username: user, password: hash}).into(auth::table()).execute(&conn) {
        Ok(_) => Ok(()),
        Err(_) => Err(AuthError::DBError)
    }
}

struct UnhashedUser {
    pub username: String,
    pub password: String
}
impl UnhashedUser {
    //Checks if user password matches
    pub fn auth(&self, auth_user: &User) -> Result<(),AuthError> {
        if self.username.starts_with("Guest") {
            return Err(AuthError::UsernameReserved);
        }
        //check password 
        let mut hasher = Sha3_256::default();
        hasher.input(self.password.as_bytes());
        if self.username == auth_user.username {
            if auth_user.password.as_slice() == &hasher.result()[..] {
            //Password verified 
                Ok(())
            } else {
                //Wrong password
                Err(AuthError::BadPassword)
            }
        } else {
            Err(AuthError::InvalidUsername)
        }
    }
}
#[cfg(test)]
mod test {
    use super::{UnhashedUser,AuthError};
    use super::models::User;
    #[test]
    fn auth_test() {
        UnhashedUser { username: "user".to_owned(), password: "password".to_owned()}
                .auth(&User {
                    id: 0, 
                    username: "user".to_owned(), 
                    password: vec![0xc0,0x06,0x7d,0x4a,0xf4,0xe8,0x7f,0x00,0xdb,0xac,0x63,0xb6,0x15,0x68,0x28,0x23,0x70,0x59,0x17,0x2d,0x1b,0xbe,0xac,0x67,0x42,0x73,0x45,0xd6,0xa9,0xfd,0xa4,0x84]
                }).unwrap();
    }
    #[test]
    fn bad_user_test() {
        assert_eq!((UnhashedUser { username: "not_user".to_owned(), password: "password".to_owned()})
                .auth(&User {
                    id: 0, 
                    username: "user".to_owned(), 
                    password: vec![0xc0,0x06,0x7d,0x4a,0xf4,0xe8,0x7f,0x00,0xdb,0xac,0x63,0xb6,0x15,0x68,0x28,0x23,0x70,0x59,0x17,0x2d,0x1b,0xbe,0xac,0x67,0x42,0x73,0x45,0xd6,0xa9,0xfd,0xa4,0x84]
                }),Err(AuthError::InvalidUsername));
    }
    #[test]
    fn bad_password_test() {
        assert_eq!((UnhashedUser { username: "user".to_owned(), password: "not_password".to_owned()})
                .auth(&User {
                    id: 0, 
                    username: "user".to_owned(), 
                    password: vec![0xc0,0x06,0x7d,0x4a,0xf4,0xe8,0x7f,0x00,0xdb,0xac,0x63,0xb6,0x15,0x68,0x28,0x23,0x70,0x59,0x17,0x2d,0x1b,0xbe,0xac,0x67,0x42,0x73,0x45,0xd6,0xa9,0xfd,0xa4,0x84]
                }),Err(AuthError::BadPassword));
    }
    #[test]
    fn reserved_user_test() {
        assert_eq!((UnhashedUser { username: "Guest".to_owned(), password: "not_password".to_owned()})
                .auth(&User {
                    id: 0, 
                    username: "user".to_owned(), 
                    password: vec![0xc0,0x06,0x7d,0x4a,0xf4,0xe8,0x7f,0x00,0xdb,0xac,0x63,0xb6,0x15,0x68,0x28,0x23,0x70,0x59,0x17,0x2d,0x1b,0xbe,0xac,0x67,0x42,0x73,0x45,0xd6,0xa9,0xfd,0xa4,0x84]
                }),Err(AuthError::UsernameReserved));
    }
    #[test]
    fn reserved_user_with_additional_stuff_test() {
        assert_eq!((UnhashedUser { username: "Guest55319234".to_owned(), password: "not_password".to_owned()})
                .auth(&User {
                    id: 0, 
                    username: "user".to_owned(), 
                    password: vec![0xc0,0x06,0x7d,0x4a,0xf4,0xe8,0x7f,0x00,0xdb,0xac,0x63,0xb6,0x15,0x68,0x28,0x23,0x70,0x59,0x17,0x2d,0x1b,0xbe,0xac,0x67,0x42,0x73,0x45,0xd6,0xa9,0xfd,0xa4,0x84]
                }),Err(AuthError::UsernameReserved));
    }
}
