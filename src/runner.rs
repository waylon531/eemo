//    eemo, a server for an electrical engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use libeemo::WorldMessage as Message;
use world::World;
use std::sync::{Arc,RwLock};
use std::sync::mpsc::Receiver;
use std::thread;
use std::time::{Duration,Instant};
//Pretty conservative max_ticks, maybe change
const MAX_TICKS: u32 = 480;
pub struct Runner {
    world: Arc<RwLock<World>>
}
impl Runner {
    pub fn new(world: World) -> Runner {
        Runner{ world: Arc::new(RwLock::new(world)) }
    }
    pub fn get_world(&self) -> Arc<RwLock<World>> {
        self.world.clone()
    }
    pub fn run(&mut self, recv: Receiver<Message> ) -> ! {
        let mut ticks = 120;
        loop {
            let first_now = Instant::now();
            {
                match self.world.write() {
                    Err(e) => panic!("Runner thread poisoned: {}",e),
                    Ok(mut world_guard) => {
                        world_guard.tick();
                        for v in recv.try_iter() {
                            match v {
                                Message::PlayerMove(s,pos) => world_guard.update_player(s,pos),
                                Message::ObjectAdd(s,pos) => world_guard.object_add(s,pos),
                                Message::ObjectDel(pos) => world_guard.object_del(pos),
                                Message::Activate(pos,data) => world_guard.activate(pos,data),
                            }
                        }
                    }
                }
            }
            let now = Instant::now();
            let time_taken = now.duration_since(first_now);
            let max_time = Duration::from_secs(1)/ticks;
            if max_time > time_taken {
                thread::sleep(max_time - time_taken);
                //Double ticks if there is 4 times the amount of time available
                if max_time/4 > time_taken && ticks < MAX_TICKS {
                    ticks = ticks * 2;
                }
            } else {
                //Halve ticks if operation did not complete in time
                ticks = ticks / 2;
            }
        }
    }
}
