//    eemo, a server for an electrical engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use libeemo::{ObjectData,Direction};
use world::{Cable,Cables,Tickable};


#[derive(PartialEq,Eq,Clone)]
pub struct Lever {
    state: bool
}

impl Lever {
    pub fn new(state: bool) -> Lever {
        Lever { state }
    }
}

impl Tickable for Lever {
    fn get_outputs(&self) -> Cables {
        let mut cables = Cables::new();
        if self.state {
            cables.north = [true; 32]
        }
        cables
    }
    fn set_inputs(&mut self,_data: Cable,_side: Direction) {
    }
    
    fn tick(&mut self) {
    }
    fn send(&self) -> ObjectData {
        ObjectData::Lever(self.state)
    }
    fn activate(&mut self, _: u32) {
        //Toggle lever
        self.state = !self.state;
    }
}

#[derive(PartialEq,Eq,Clone)]
pub struct Light {
    state: bool,
    inputs: Cables
}

impl Light {
    pub fn new() -> Light {
        Light { state: false, inputs: Cables::new() }
    }
}

impl Tickable for Light {
    //Should I have lamps pass on their signal?
    //It sees like a good idea
    fn get_outputs(&self) -> Cables {
        self.inputs
    }
    fn set_inputs(&mut self,data: Cable,side: Direction) {
        self.inputs.set_side(side,data);
    }
    
    fn tick(&mut self) {
        //True is there is a true input on south side
        //false if there is no such input
        self.state = self.inputs.south.contains(&true);
        let south_inputs = self.inputs.south;
        self.inputs.set_side(Direction::North,south_inputs);
        self.inputs.south = [false; 32];
    }
    fn send(&self) -> ObjectData {
        ObjectData::Light(self.state)
    }
    fn activate(&mut self, _: u32) {
    }
}
