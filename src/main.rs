//    eemo, a server for an electrical engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

const ADDRESS: &'static str = "0.0.0.0:44938";
#[macro_use]
extern crate slog;
extern crate slog_envlogger;
extern crate slog_term;
extern crate slog_async;

#[macro_use] extern crate diesel_codegen;
#[macro_use] extern crate diesel;
extern crate dotenv;
extern crate getopts;
extern crate libeemo;
extern crate rustls;
extern crate sha3;
extern crate rand;

#[cfg(not(feature = "fake"))]
mod auth;
mod error;
mod event;
mod gate;
mod io_object;
mod ring;
mod runner;
mod stream;
mod world;

use libeemo::{ClientToServerMessage,WorldMessage};

#[cfg(not(feature = "fake"))]
use event::ClientState;
use error::Error;
use ring::{Ring,RingContainer};
use runner::Runner;
use stream::StreamContainer;
use world::World;

use std::fs::File;
use std::env;
use std::io::{ErrorKind,BufReader,Read,Write};
use std::net::{TcpListener,TcpStream};
use std::time::Duration;
use std::thread;
use std::sync::{Arc,RwLock,Mutex};
use std::sync::mpsc::{channel,Sender};

use getopts::Options;

use rustls::{ServerConfig,ServerSession};

use slog::{Logger,Drain};

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}

fn main() {
//TODO: spawn autosave thread
//TODO: take cert and save file from command line

    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.reqopt("c", "cert", "set certificate file", "NAME");
    opts.reqopt("k", "key", "set key file", "NAME");
    opts.optopt("s", "save", "set autosave file", "NAME");
    opts.optflag("h", "help", "print this help menu");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };
    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }
    let cert = match matches.opt_str("c") {
        Some(cert) => cert,
        None => {
            println!("Certificate file needed, specify with --cert or -c");
            print_usage(&program, opts);
            return;
        }
    };
    let key = match matches.opt_str("k") {
        Some(key) => key,
        None => {
            println!("Key file needed, specify with --key or -k");
            print_usage(&program, opts);
            return;
        }
    };
    let save_file = match matches.opt_str("s") {
        Some(save) => save,
        //TODO: default to something sensible
        None => {"eemo.autosave".to_owned()} //panic!("Autosave file needed, specify with --save or -s")
    };


    let drain =
        slog_async::Async::default(
            slog_envlogger::new(
                slog_term::CompactFormat::new(
                    slog_term::TermDecorator::new()
                    .stderr().build()
                    ).build().fuse()
                ));
    let root_logger = slog::Logger::root(drain.fuse(),slog_o!());
    info!(root_logger,"Starting");

    //initialize world (TODO: load from file)
    let world = World::new();

    debug!(root_logger,"World created");

    let listener = TcpListener::bind(ADDRESS).unwrap();
    debug!(root_logger, "Bound to {}",ADDRESS);

    let mut config = ServerConfig::new();

    info!(root_logger,"Loading certificate: {}", &cert);
    //Load certfile
    let mut certfile = BufReader::new(File::open(cert).expect("Cannot access cert"));
    let cert_bytes = rustls::internal::pemfile::certs(&mut certfile).unwrap();


    info!(root_logger,"Loading keyfile: {}",&key);
    let mut keyfile = BufReader::new(File::open(&key).expect("Cannot access key file"));
    let mut key_bytes = rustls::internal::pemfile::pkcs8_private_keys(&mut keyfile).unwrap();
    if key_bytes.len() == 0 {
        let mut keyfile = BufReader::new(File::open(key).expect("Cannot access key file"));
        key_bytes = rustls::internal::pemfile::pkcs8_private_keys(&mut keyfile).unwrap();
        if key_bytes.len() == 0 {
            error!(root_logger,"No keys found in key file!");
            return;
        }
    }

    config.set_single_cert(cert_bytes,key_bytes[0].clone());
    let rc_config = Arc::new(config);

    //Create chat message buffer
    let message = Ring::new();
    let message_container = Arc::new(RwLock::new(message));
    debug!(root_logger,"Message buffer created");

    //Adapter thread
    //Buffer that gets read out
    //Maybe channels?

    info!(root_logger,"Starting game runner");
    //Runner thread
    //World is in RWLock
    let mut runner = Runner::new(world);
    let arc_world = runner.get_world();
    let (runner_tx,rx) = channel();
    thread::spawn(move || runner.run(rx));

    //Chat propogate thread
    //Client threads send messages to this thread,
    //it sends a message out to all client threads
    //let (chat_send,rx) = channel();
    //thread::spawn(move || chat_runner(rx));

    info!(root_logger,"Listening for clients");
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                let messages = RingContainer::new(message_container.clone());
                let config_clone = rc_config.clone();
                let runner_tx_clone = runner_tx.clone();
                let world = arc_world.clone();
                let logger = root_logger.new(
                    o!(
                        "IP" => format!("{}",stream.peer_addr().unwrap()),
                        "context" => "main client thread")
                    );
                debug!(logger,"Connection established");
                thread::spawn(move || handle_client(logger,stream,messages,config_clone,runner_tx_clone,world));
            }
            Err(e) => { debug!(root_logger,"Connection attempt failed";"error" => format!("{}",e)); }
        }
    }

}
fn handle_client(logger: Logger, stream_raw: TcpStream, messages: RingContainer,rc_config: Arc<ServerConfig>, world_channel: Sender<WorldMessage>, world: Arc<RwLock<World>> ) 
    {
    //TODO: clean up expects

	stream_raw.set_read_timeout(Some(Duration::from_millis(1))).expect("Failed to set read timeout");


    let logger_child = logger.new(o!("context" => "event handler"));
    let mut state = ClientState::new(logger_child,world,world_channel,messages.clone());

    let session_container = Arc::new(
        Mutex::new(
            StreamContainer::new(
                ServerSession::new(&rc_config),
                stream_raw
                )
            )
        );

    let session_clone = session_container.clone();
    let messages_clone = messages.clone();
    let child_logger = logger.new(o!("context" => "message handler"));
    thread::spawn(move || message_sender(child_logger,session_clone,messages_clone) );
    trace!(logger,"Spawned message handler");
    let mut buffer = Vec::new();
    trace!(logger,"Starting event handler");
    'a: loop {
        trace!(logger,"Locking session");
        let ref mut session = *session_container.lock().expect("Client handler thread poisoned");
        //Write all TLS data to client
        trace!(logger, "Flushing TLS data to client");
        session.flush().unwrap();
        trace!(logger, "TLS data flushed");

        //This should pretty much always be true
        if session.wants_read() {
            match session.do_tls_read() {
                Ok(0) => {
                    //Client hung up
                    debug!(logger,"Client closed connection");
                    //TODO: clean up user
                    // and remove them if they are a guest
                    return;
                },
                Ok(_) => { 
                    //Got some bytes
                    //println!("Got {} bytes",num);
                    //Good to do stuff
                    //This is triggered when clients connect
                    //for i in 0 .. 100 {
                    //    //Might exhaust stuff? UGH
                    //    println!("AT: {}",i);
                    //    if session.wants_read() {
                    //        do_tls_read(session,stream).unwrap();
                    //    } else {
                    //        println!("GOT TO: {}",i);
                    //        break
                    //    }

                    //}


                    session.read_to_end(&mut buffer).expect("Couldn't read to buffer");
                    if buffer.len() == 0 {
                        trace!(logger,"Zero-length buffer received");
                        continue 'a;
                    } 
                    
                    trace!(logger,"Trying to deserializing buffer");
                    //Try deserializing a message from the message buffer
                    let message = match ClientToServerMessage::from_vec(&mut buffer) {
                        Ok(m) => m,
                        Err(_) => {

                            //println!("Syntax error {:?}",e);
                            //buffer.clear();
                            continue 'a;
                        }
                    };
                    debug!(logger,"Got message from client"; "message" => format!("{:?}",message));
                    let session_clone = session_container.clone();
                    trace!(logger, "Processing message");
                    //Process event
                    state.process_message(message, session_clone, session).unwrap();
                },
                Err(Error::IOError(e)) => {
                    match e.kind() {
                        //If the error is that it's timed out
                        //then eveything is fine and we can NOP
                        ErrorKind::WouldBlock => {},	
                        ErrorKind::TimedOut => {},	
                        _ => panic!("Error reading from TCP stream into TLS session: {}",e)
                    }
                },
                Err(e) => {
                    panic!("ERROR: {:?}", e);
                }
            }
        } else {
            thread::sleep(Duration::from_millis(1));
        }
    }
}
fn message_sender(logger: Logger, session_container: Arc<Mutex<StreamContainer>>,mut messages: RingContainer) {
    debug!(logger,"Starting chat message handler");
    loop {
        match messages.get_messages() {
            None => {},
            Some(v) => {
                trace!(logger,"Locking session");
                let ref mut session = *session_container.lock().expect("Failed to lock TcpStream in chat message sender");
                trace!(logger,"Locked session");
                for message in v {
                    message.write_out(session)
                        .expect("Unable to write message to TLS session");
                    debug!(logger,"Sent chat message to client"; "chat message" => format!("{:?}",message));
                }
            }
        }
        //Maybe TODO: sleep for longer to prevent stupid locking
        thread::sleep(Duration::from_millis(1));
    }
}
