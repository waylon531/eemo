//    eemo, a server for an electrical engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
use std::io;
use std::convert::From;
//use std::sync::{PoisonError,MutexGuard};
use rustls;
use libeemo;
#[derive(Debug)]
pub enum Error {
    IOError(io::Error),
    TLSError(rustls::TLSError),
    EncodeError(libeemo::encode::Error),
    DecodeError(libeemo::decode::Error),
    //PoisonError(PoisonError),
} 
impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::IOError(e)
    }
}
impl From<rustls::TLSError> for Error {
    fn from(e: rustls::TLSError) -> Error {
        Error::TLSError(e)
    }
}
impl From<libeemo::encode::Error> for Error {
    fn from(e: libeemo::encode::Error) -> Error {
        Error::EncodeError(e)
    }
}
impl From<libeemo::decode::Error> for Error {
    fn from(e: libeemo::decode::Error) -> Error {
        Error::DecodeError(e)
    }
}
//impl<W> From<PoisonError<W>> for Error {
//    fn from(e: PoisonError<W>) -> Error {
//        Error::PoisonError(e)
//    }
//}
