//    eemo, a server for an electrical engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use error::Error;

use std::io::{self,Read,Write};
use std::net::TcpStream;
use rustls::{ServerSession,Session};

pub struct StreamContainer {
    session: ServerSession,
    stream: TcpStream
}
impl StreamContainer {
    pub fn new(session: ServerSession, stream: TcpStream) -> StreamContainer {
        StreamContainer {
            session,
            stream
        }
    }
    pub fn do_tls_read(&mut self) -> Result<usize,Error> {
        // Read some TLS data.
        let num = self.session.read_tls(&mut self.stream)?;
        //maybe wrap in my own error type
        self.session.process_new_packets()?;
        Ok(num)
    }
    pub fn wants_read(&self) -> bool {
        self.session.wants_read()
    }
}
impl Write for StreamContainer {
    fn flush(&mut self) -> io::Result<()> {
        //Flush data out over the wire
        while self.session.wants_write() {
            self.session.write_tls(&mut self.stream)?;
        }
        Ok(())
    }
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        //Write buffer to session
        self.session.write(buf)
    }
}
impl Read for StreamContainer {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.session.read(buf)
    }
}
