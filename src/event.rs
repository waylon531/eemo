//    eemo, a server for an electrical engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use auth::{auth_user,add_user};
use error::Error;
use ring::RingContainer;
use stream::StreamContainer;
use world::World;

use rand;

use std::sync::{Arc,RwLock,Mutex};
use std::sync::mpsc::Sender;
use std::thread;

use slog::Logger;

use libeemo::{ClientToServerMessage,ServerToClientMessage,Position,WorldMessage};

pub struct ClientState {
    username: String,
    world: Arc<RwLock<World>>,
    world_channel: Sender<WorldMessage>,
    messages: RingContainer,
    logger: Logger
}
impl ClientState {
    pub fn new(logger: Logger, world: Arc<RwLock<World>>, world_channel: Sender<WorldMessage>, messages: RingContainer) -> ClientState {
        ClientState {
            username: "Guest".to_owned() + ( rand::random::<u32>() ).to_string().as_str(),
            world,
            world_channel,
            messages,
            logger
        }
    }
    pub fn process_message(
            &mut self, 
            message: ClientToServerMessage, 
            session_arc: Arc<Mutex<StreamContainer>>,
            //I need both a StreamContainer and the Arc pointing to it
            //so I can pass the Arc on to child threads
            // and read+write to the session
            session: &mut StreamContainer
        ) -> Result<(),Error> {

        trace!(self.logger,"Processing message");
        match message {
            ClientToServerMessage::Move(pos) => self.world_channel.send(WorldMessage::PlayerMove(self.username.clone(),pos)).expect("Failed to send data to world"),
            ClientToServerMessage::GetPosition => {
                let pos = match self.world.read().expect("Failed to lock world").get_player_position(&self.username) {
                    Some(p) => p,
                    None => Position(0,0)
                };
                ServerToClientMessage::YourPosition(pos)
                    .write_out(session)?;

            },
            ClientToServerMessage::GetWorld => {
                let session_clone = session_arc.clone();
                let world_clone = self.world.clone();
                let log_child = self.logger.new(o!("context" => "whole-world sender"));
                thread::spawn(move || world_sender(log_child,session_clone,world_clone));
            },
            ClientToServerMessage::GetWorldSubset(bot_left,top_right) => {
                let session_clone = session_arc.clone();
                let world_clone = self.world.clone();
                let log_child = self.logger.new(o!("context" => "restricted world sender"));
                thread::spawn(move || world_sender_restricted(log_child,session_clone,world_clone,bot_left,top_right));
                //world_sender_restricted(session,self.world);
            },
            ClientToServerMessage::ObjectAdd(obj,pos) => self.world_channel.send(WorldMessage::ObjectAdd(obj,pos)).expect("Failed to send data to world"),
            ClientToServerMessage::ObjectDel(pos) => self.world_channel.send(WorldMessage::ObjectDel(pos)).expect("Failed to send data to world"),
            ClientToServerMessage::Activate(pos,data) => self.world_channel.send(WorldMessage::Activate(pos,data)).expect("Failed to send data to world"),
            ClientToServerMessage::ChatMessage(message) => { 
                self.messages.push_message(message,self.username.clone()); 
            },
            ClientToServerMessage::GetSource => {
                //TODO: make this happen as a config option
                ServerToClientMessage::Source("https://gitlab.com/waylon531/eemo".to_owned())
                    .write_out(session)?;

            },
            ClientToServerMessage::GetName => {
                //TODO: make this happen as a config option
                ServerToClientMessage::Name(self.username.clone())
                    .write_out(session)?;

            },
            ClientToServerMessage::Auth(user,pass) => {
                #[cfg(not(feature = "fake"))]
                {
                let result = auth_user(user.clone(),pass);
                if result.is_ok() {
                    self.username = user;
                }
                ServerToClientMessage::AuthResponse(result)
                    .write_out(session)?;
                }
            },
            ClientToServerMessage::Register(user,pass) => {
                #[cfg(not(feature = "fake"))]
                {
                let result = add_user(user,pass);
                ServerToClientMessage::AuthResponse(result)
                    .write_out(session)?;
                }
            }
        }
        Ok(())
    }
}
fn world_sender(logger: Logger, session_container: Arc<Mutex<StreamContainer>>,world: Arc<RwLock<World>>) {
    trace!(logger,"Locking world");
    let ref world = *world.read().expect("Failed to lock World in world_sender");
    trace!(logger,"Locked world");
    let message = ServerToClientMessage::Game(world.send());
    {
        trace!(logger,"Locking session");
        let ref mut session = *session_container.lock().expect("Failed to lock TcpStream in world_sender");
        trace!(logger,"Locked session");
        message.write_out(session)
            .expect("Unable to write message to TLS session");
        trace!(logger, "Sent world");
    }
}
fn world_sender_restricted(logger: Logger,session_container: Arc<Mutex<StreamContainer>>,world: Arc<RwLock<World>>,bottom_left: Position, top_right: Position) {
    trace!(logger, "Locking world");
    let ref world = *world.read().expect("Failed to lock World in world_sender");
    trace!(logger, "Locked world");
    let message = ServerToClientMessage::Game(world.send_restricted(bottom_left,top_right));
    {
        trace!(logger,"Locking session");
        let ref mut session = *session_container.lock().expect("Failed to lock TcpStream in world_sender");
        trace!(logger,"Locked session");
        message.write_out(session)
            .expect("Unable to write message to TLS session");
        trace!(logger, "Sent world");
    }
}
