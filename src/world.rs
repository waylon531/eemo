//    eemo, a server for an electrical engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::collections::HashMap;
use libeemo::{Position,Object,SendWorld,ObjectData,Direction};
use gate::GateWrapper;
use io_object::{Lever,Light};

pub struct World {
    players: HashMap<String,Position>,
    objects: HashMap<Position, (Direction, Box<Tickable + Send + Sync>)>
}
impl World {
    pub fn new () -> World {
        World {
            players: HashMap::new(),
            objects: HashMap::new()
        }
    }
    pub fn send(&self) -> SendWorld{
        SendWorld {
            players: self.players.clone(),
            objects: self.objects.iter()
                .map(
                    |(k,v)| 
                    (
                        *k,
                        Object { orientation : v.0,
                                  object_data: v.1.send()
                        }
                    )
                )
                .collect()
        }
    }   
    pub fn send_restricted(&self, lower_left: Position, upper_right: Position) -> SendWorld{
        SendWorld {
            players: self.players.clone(),
            objects: self.objects.iter()
                .filter(|&(k,_)| 
                        k.0 >= lower_left.0 && k.1 >= lower_left.1 
                        && k.0 <= upper_right.0 && k.1 <= upper_right.1)
                .map(
                    |(k,v)| 
                    (
                        *k,
                        Object { orientation : v.0,
                                  object_data: v.1.send()
                        }
                    )
                )
                .collect()
        }
    }   
    pub fn get_player_position(&self, name: &String) -> Option<Position> {
        match self.players.get(name) {
            Some(pos) => Some(*pos),
            None => None
        }
    }
    pub fn update_player(&mut self, name: String, pos: Position) {
        self.players.insert(name,pos);
    }
    pub fn object_add(&mut self, object: Object, pos: Position) {
        let object_name = object.object_data;
        let orientation = object.orientation;
        self.objects.insert(pos,(orientation,match object_name {
            ObjectData::Gate(gate) => {
                Box::new(GateWrapper::new(gate))
            },
            ObjectData::Lever(state) => {
                Box::new(Lever::new(state))
            },
            ObjectData::Light(_) => {
                Box::new(Light::new())
            },

        }));
    }
    pub fn object_del(&mut self, pos: Position) {
        self.objects.remove(&pos);
    }
    pub fn activate(&mut self, pos: Position, data: u32) {
        match self.objects.get_mut(&pos) {
            Some(object) => object.1.activate(data),
            None => {}
        }
    }
    pub fn tick(&mut self) {
        let mut keys: Vec<Position> = Vec::new();
        //TODO: split into chunks and do multi-threading
        for (k,v) in self.objects.iter_mut() {
            v.1.tick();
            keys.push(*k);
        }
        for key in keys {
            let outputs;
            let orientation;
            {
                let object = self.objects.get(&key).expect("Key not found in map");
                outputs =  object.1.get_outputs();
                orientation = object.0;
            }
            let adjacent_data = match key {
                //Hardcoding best coding
                pos @ Position(_,_) => [
                    //Node is north of current node, but set south inputs
                    (Direction::South + orientation, pos + ( Direction::North + orientation ),outputs.north),
                    (Direction::North + orientation, pos + (  Direction::South + orientation),outputs.south),
                    (Direction::West + orientation, pos + (Direction::East + orientation),outputs.east),
                    (Direction::East + orientation, pos + (Direction::West + orientation),outputs.west)]
            };
            for node in adjacent_data.iter() {
                match node {
                    &(ref side,ref key,ref data) => match self.objects.get_mut(key) {
                        Some(object) => {
                            let orientation = object.0;
                            object.1.set_inputs(*data,*side + orientation);
                        },
                        //Doing nothing is okay here
                        None => {}
                    }
                }
            }   
        }
    }
}
pub trait Tickable : TickableClone {
    fn tick(&mut self);
    fn get_outputs(&self) -> Cables;
    //64 inputs on each side,
    fn set_inputs(&mut self,Cable,Direction);
    fn activate(&mut self,u32);
    fn send(&self) -> ObjectData;
}
pub trait TickableClone {
    fn clone_box(&self) -> Box<Tickable>;
}

//Why does this require static
//static = no references
impl<T> TickableClone for T where T: 'static + Tickable + Clone {
    fn clone_box(&self) -> Box<Tickable> {
        Box::new(self.clone())
    }
}

// We can now implement Clone manually by forwarding to clone_box.
impl Clone for Box<Tickable> {
    fn clone(&self) -> Box<Tickable> {
        self.clone_box()
    }
}

#[derive(Eq,PartialEq,Copy,Clone)]
pub struct Cables {
    pub north: Cable,
    pub south: Cable,
    pub east: Cable,
    pub west: Cable,
}
impl Cables {
    pub fn new() -> Cables {
        Cables {
            north: [false; 32],
            south: [false; 32],
            east: [false; 32],
            west: [false; 32],
        }
    }
    pub fn set_side(&mut self,side: Direction,data: Cable) {
        match side {
            Direction::North => self.north = data,
            Direction::South => self.south = data,
            Direction::East => self.east = data,
            Direction::West => self.west = data,
        }
    }
}
pub type Cable = [bool; 32];

