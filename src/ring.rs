//    eemo, a server for an electrical engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std::sync::{Arc,RwLock};

use libeemo::ServerToClientMessage as ServerVerb;


const BUFFER_SIZE: u16 = 512;
#[derive(Clone)]
pub struct RingContainer {
   ring: Arc<RwLock<Ring>>,
   counter: u16
}
impl RingContainer {
    pub fn new(ring: Arc<RwLock<Ring>>) -> RingContainer {
        let counter = {
            (*ring).read().unwrap().get_counter()
        };
        RingContainer {
            counter: counter,
            ring: ring,
        }
    }
    pub fn get_messages(&mut self) -> Option<Vec<ServerVerb>> {
        let ring = (*self.ring).read().unwrap();
        if self.counter != ring.get_counter() {
            let mut vec = Vec::new();
            loop {
                self.counter = (self.counter + 1) % BUFFER_SIZE;
                vec.push(ring.get_message(self.counter));
                if self.counter == ring.get_counter() {
                    break
                }
            }
            Some(vec)
        } else {
            None
        }

    }
    //I wonder if there's a way to do this without locking
    //Maybe atomics
    //pub fn check_messages(&self) -> bool {
    //    self.counter != (*self.ring).lock().unwrap().get_counter()
    //}
    pub fn push_message(&mut self,message: String,username: String) {
        (*self.ring).write().unwrap().push_message(message,username);
    }
    //pub fn push(&mut self,verb: ServerVerb) {
    //    (*self.ring).write().unwrap().push(verb);
    //}
}
pub struct Ring {
    buffer: [Option<ServerVerb>; BUFFER_SIZE as usize],
    counter: u16
}
impl Ring {
    //PRAISE VIM
    pub fn new() -> Ring {
        Ring {counter: BUFFER_SIZE-1, buffer:
        [
            None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,None,
        ]
        }
    }
    pub fn push_message(&mut self,message: String,username: String) {
        self.counter = ( self.counter + 1 ) % BUFFER_SIZE;
        self.buffer[self.counter as usize] = Some(ServerVerb::ChatMessage(message,username));
    }
    //pub fn push(&mut self,verb: ServerVerb) {
    //    self.counter = ( self.counter + 1 ) % BUFFER_SIZE;
    //    self.buffer[self.counter as usize] = Some(verb);
    //}
    //Returns index of last written message
    pub fn get_counter(&self) -> u16 {
        self.counter
    }
    pub fn get_message(&self,index: u16) -> ServerVerb {
        match self.buffer[index as usize].clone() {
            Some(message) => message,
            //I probably shouldn't silently fail?
            None => ServerVerb::ChatMessage("".to_owned(),"".to_owned())
        }
    }
}
#[cfg(test)]
mod test {
    use std::sync::{Arc,RwLock};
    use super::{Ring,RingContainer};
    use super::BUFFER_SIZE;
    use libeemo::ServerToClientMessage as ServerVerb;
    #[test]
    fn single_push() {
        let mut ring = Ring::new();
        ring.push_message("THIS IS A TEST".to_owned(),"Server".to_owned());
        assert_eq!(ring.get_message(ring.get_counter()),ServerVerb::ChatMessage("THIS IS A TEST".to_owned(),"Server".to_owned()));
    }
    #[test]
    fn no_push() {
        let ring = Ring::new();
        assert_eq!(ring.get_message(ring.get_counter()),ServerVerb::ChatMessage("".to_owned(),"".to_owned()));
    }
    #[test]
    #[should_panic]
    fn push_128() {
        let mut ring = Ring::new();
        ring.push_message("THIS IS A TEST".to_owned(),"Server".to_owned());
        for _ in 0 .. 127 {
            ring.push_message("NOT A TEST".to_owned(),"NOBODY".to_owned());
        }
        assert_eq!(ring.get_message(0),ServerVerb::ChatMessage("THIS IS A TEST".to_owned(),"Server".to_owned()));
        ring.push_message("NOT A TEST".to_owned(),"HAL".to_owned());
        assert_eq!(ring.get_message(0),ServerVerb::ChatMessage("NOT A TEST".to_owned(),"HAL".to_owned()));
    }
    #[test]
    fn round_push() {
        let mut ring = Ring::new();
        ring.push_message("THIS IS A TEST".to_owned(),"Server".to_owned());
        for _ in 0 .. BUFFER_SIZE-1 {
            ring.push_message("NOT A TEST".to_owned(),"NOBODY".to_owned());
        }
        assert_eq!(ring.get_message(0),ServerVerb::ChatMessage("THIS IS A TEST".to_owned(),"Server".to_owned()));
        ring.push_message("NOT A TEST".to_owned(),"HAL".to_owned());
        assert_eq!(ring.get_message(0),ServerVerb::ChatMessage("NOT A TEST".to_owned(),"HAL".to_owned()));
    }
    #[test]
    fn ring_container_test() {
        let ring = Ring::new();
        let container = Arc::new(RwLock::new(ring));
        let mut container1 = RingContainer::new(container.clone());
        container.write().unwrap().push_message("THIS IS A TEST".to_owned(),"Server".to_owned());
        let mut container2 = RingContainer::new(container.clone());
        assert_eq!(container2.get_messages(),None);
        container2.push_message("NOT A TEST".to_owned(),"NOBODY".to_owned());
        assert_eq!(container1.get_messages().unwrap(),vec![ServerVerb::ChatMessage("THIS IS A TEST".to_owned(),"Server".to_owned()),ServerVerb::ChatMessage("NOT A TEST".to_owned(),"NOBODY".to_owned())]);
    }
    #[test]
    #[should_panic]
    fn ring_128_push_container_test() {
        let ring = Ring::new();
        let container = Arc::new(RwLock::new(ring));
        let mut container1 = RingContainer::new(container.clone());
        let mut container2 = RingContainer::new(container.clone());
        assert_eq!(container2.get_messages(),None);
        for _ in 0 .. 128 {
            container2.push_message("NOT A TEST".to_owned(),"NOBODY".to_owned());
        }
        assert_eq!(container1.get_messages(),None);
    }
    #[test]
    fn ring_round_push_container_test() {
        let ring = Ring::new();
        let container = Arc::new(RwLock::new(ring));
        let mut container1 = RingContainer::new(container.clone());
        let mut container2 = RingContainer::new(container.clone());
        assert_eq!(container2.get_messages(),None);
        for _ in 0 .. BUFFER_SIZE {
            container2.push_message("NOT A TEST".to_owned(),"NOBODY".to_owned());
        }
        assert_eq!(container1.get_messages(),None);
    }
    #[test]
    fn ring_127_push_container_test() {
        let ring = Ring::new();
        let container = Arc::new(RwLock::new(ring));
        let mut container1 = RingContainer::new(container.clone());
        let mut container2 = RingContainer::new(container.clone());
        assert_eq!(container2.get_messages(),None);
        for _ in 0 .. 127 {
            container2.push_message("NOT A TEST".to_owned(),"NOBODY".to_owned());
        }
        assert_eq!(container1.get_messages().unwrap().len(),127);
    }
}
