//    eemo, a server for an electrical engineering mmo
//    Copyright (C) 2017 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use libeemo::{Gate,ObjectData,Direction};
use world::{Cable,Cables,Tickable};


#[derive(PartialEq,Eq,Clone)]
pub struct GateWrapper {
    gate: Gate,
    cables: Cables,
}

impl GateWrapper {
    pub fn new(gate: Gate) -> GateWrapper {
        GateWrapper { gate: gate, cables: Cables::new() }
    }
}

impl Tickable for GateWrapper {
    fn get_outputs(&self) -> Cables {
        self.cables
    }
    fn set_inputs(&mut self,data: Cable,side: Direction) {
        self.cables.set_side(side,data);
    }
    
    fn tick(&mut self) {
        match self.gate {
            Gate::And => {
                for i in 0..32 {
                    let result = self.cables.east[i] && self.cables.west[i];
                    self.cables.north[i] = result;
                    self.cables.south[i] = !result;
                }
            },
            Gate::Or => {
                for i in 0..32 {
                    let result = self.cables.east[i] || self.cables.west[i];
                    self.cables.north[i] = result;
                    self.cables.south[i] = !result;
                }
            },
            Gate::Xor => {
                for i in 0..32 {
                    if self.cables.east[i] == true || self.cables.west[i] == true {
                        if self.cables.east[i] == true && self.cables.west[i] == true {
                            self.cables.north[i] = false;
                            self.cables.south[i] = true;
                        } else {
                            self.cables.north[i] = true;
                            self.cables.south[i] = false;
                        }
                    } else {
                        self.cables.north[i] = false;
                        self.cables.south[i] = true;
                    }
                }
            },
        }
        //Reset cables to default state
        self.cables.west = [false; 32];
        self.cables.east = [false; 32];
    }
    fn send(&self) -> ObjectData {
        ObjectData::Gate(self.gate)
    }
    fn activate(&mut self, _: u32) {
    }
}

#[cfg(test)]
mod test {
    use libeemo::{Gate,Direction};
    use world::Tickable;
    use super::GateWrapper;
    #[test]
    fn and_tick() {
        let mut gate = GateWrapper::new(Gate::And);
        gate.set_inputs([true,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false],Direction::East);
        gate.set_inputs([true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true],Direction::West);
        gate.tick();
        let outputs = gate.get_outputs();
        assert_eq!(outputs.north,[true,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]);
        assert_eq!(outputs.south,[false,true,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true]);
    }
    #[test]
    fn xor_tick() {
        let mut gate = GateWrapper::new(Gate::Xor);
        gate.set_inputs([true,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false], Direction::East);
        gate.set_inputs([true,true, false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false], Direction::West);
        gate.tick();
        let outputs = gate.get_outputs();
        assert_eq!(outputs.north, [false,true,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]);
        assert_eq!(outputs.south, [true,false,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true]);
    }
    #[test]
    fn or_tick() {
        let mut gate = GateWrapper::new(Gate::Or);
      gate.set_inputs([true,false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false],Direction::East);
       gate.set_inputs([true,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true],Direction::West);
        gate.tick();
        let outputs = gate.get_outputs();
        assert_eq!(outputs.north,[true,false,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true]);
        assert_eq!(outputs.south,[false,true,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false]);
    }
}
